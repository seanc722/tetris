window.onload = function() {
	var game = new Game();
}


function Game() {
	var MOVE_DOWN_PIXELS = 2; //How many pixels a pice moves downward per redraw
	var SPEED = 20; //Redraw game speed
	var KEY_MOVE_LR_TIME = 100; //Delay between key handling
	var KEY_DROP_TIME = 200; //Delay between hard drops
	var KEY_ROTATE_TIME = 200; //Delay between rotations
	var LOCK_DELAY = 400; //Delay before locking a piece into place and merging it with board
	var BLOCK_SIZE = 20; //Size of an individual block
	var GRID_X = 10; //Game grid width in blocks
	var GRID_Y = 22; //Game grid height in blocks
	var GRID_Y_HIDDEN = 2; //Game grid blocks that are hidden above
	var BUFFER = document.getElementById("buffer"); //Canvas
	var CTX = BUFFER.getContext("2d"); //Canvas context
	
	var isRunning = false; //Is game running
	var bag = new Array(); //Holds random generated pieces (Tetris bag generator)
	var last_redraw = 0; //Time since last redraw
	var last_movement = 0; //time since last key handle
	var last_drop = 0; //Time since last hard drop
	var last_rotate = 0; //Time since last rotate
	var hold_lock = false; //Hold piece switch
	var hold_piece = null; //Piece on hold
	var lock_delay_timer = null; //timer for locking piece
	var current_piece = null; //Current piece being manipulated
	var keys = new KeyContainer(); //Holds currently held down keys
	var game_loop = null; //setInterval ID for game loop

	
	var game_grid = new Array();
	var play_area = new Rectangle((BLOCK_SIZE*5),0, BLOCK_SIZE * GRID_X, BLOCK_SIZE * (GRID_Y-GRID_Y_HIDDEN));
	

	BUFFER.width = play_area.x + play_area.width + (BLOCK_SIZE*5);
	BUFFER.height = play_area.y + play_area.height+1;
	
	
	
	/* #### PIECES AND ROTATION(SRS) ####################################### */
	var PIECE_TYPES = [
		{ // J
			start: [{x:0, y:0},{x:0, y:1}, {x:1, y:1}, {x:2, y:1}],//INTIAL POSITION! NOT PART OF ROTATION
			rotations: [
				[{x:2, y:0},{x:1, y:-1}, {x:0, y:0}, {x:-1, y:1}], //rotation 1
				[{x:0, y:2},{x:1, y:1}, {x:0, y:0}, {x:-1, y:-1}], //rotation 2
				[{x:-2, y:0}, {x:-1, y:1}, {x:0, y:0}, {x:1, y:-1}], //rotation 3
				[{x:0, y:-2},{x:-1, y:-1}, {x:0, y:0}, {x:1, y:1}] //rotation 4
			]
		},
		{ // O 
			start: [{x:1, y:0},{x:1, y:1}, {x:2, y:0}, {x:2, y:1}],
			rotations: [
				[{x:0, y:0},{x:0, y:0}, {x:0, y:0}, {x:0, y:0}] //No rotations
			]
		},
		// I
		{
			start: [{x:0, y:1}, {x:1, y:1}, {x:2, y:1}, {x:3, y:1}], //INITIAL POSITION
			rotations:[
				[{x:2, y:-1}, {x:1, y:0}, {x:0, y:1}, {x:-1, y:2}],
				[{x:1, y:2}, {x:0, y:1}, {x:-1, y:0}, {x:-2, y:-1}],
				[{x:-2, y:1}, {x:-1, y:0}, {x:0, y:-1}, {x:1, y:-2}],
				[{x:-1, y:-2}, {x:0, y:-1}, {x:1, y:0}, {x:2, y:1}]
			]
		},
		{// T
			start: [{x:0, y:1}, {x:1, y:1}, {x:1, y:0}, {x:2, y:1}], //INITIAL POSITION
			rotations: [
				[{x:1, y:-1}, {x:0, y:0}, {x:1, y:1}, {x:-1, y:1}],
				[{x:1, y:1}, {x:0, y:0}, {x:-1, y:1}, {x:-1, y:-1}],
				[{x:-1, y:1}, {x:0, y:0}, {x:-1, y:-1}, {x:1, y:-1}],
				[{x:-1, y:-1}, {x:0, y:0}, {x:1, y:-1}, {x:1, y:1}]
			]
		},
		{// S
			start: [{x:0, y:1}, {x:1, y:1}, {x:1, y:0}, {x:2, y:0}], //INITIAL POSITION
			rotations: [
				[{x:1, y:-1}, {x:0, y:0}, {x:1, y:1}, {x:0, y:2}],
				[{x:1, y:1}, {x:0, y:0}, {x:-1, y:1}, {x:-2, y:0}],
				[{x:-1, y:1}, {x:0, y:0}, {x:-1, y:-1}, {x:0, y:-2}],
				[{x:-1, y:-1}, {x:0, y:0}, {x:1, y:-1}, {x:2, y:0}]
			]
		},
		{// Z
			start: [{x:0, y:0}, {x:1, y:0}, {x:1, y:1}, {x:2, y:1}], //INITIAL POSITION
			rotations: [
				[{x:2, y:0}, {x:1, y:1}, {x:0, y:0}, {x:-1, y:1}],
				[{x:-2, y:1}, {x:-1, y:0}, {x:0, y:1}, {x:1, y:0}],
				[{x:1, y:-1}, {x:0, y:0}, {x:-1, y:-1}, {x:-2, y:0}],
				[{x:-1, y:0}, {x:0, y:-1}, {x:1, y:0}, {x:2, y:-1}]
			]
		},
		{// L
			start: [{x:0, y:1}, {x:1,y:1}, {x:2,y:1}, {x:2,y:0}],
			rotations: [
				[{x:1, y:-1}, {x:0,y:0}, {x:-1,y:1}, {x:0,y:2}],
				[{x:1, y:1}, {x:0,y:0}, {x:-1,y:-1}, {x:-2,y:0}],
				[{x:-1, y:1}, {x:0,y:0}, {x:1,y:-1}, {x:0,y:-2}],
				[{x:-1, y:-1}, {x:0,y:0}, {x:1,y:1}, {x:2,y:0}]
			]
		}
	];
	/* ### END PIECES ############################################################# */
	
	var PIECE_COLORS = {0:"#0000FF", 1:"#FFFF00", 2:"#00FFFF", 3:"#FF00FF", 4:"#00FF00", 5:"#FF0000", 6:"#FFA500"};
	
	//Handle KeyDown
	window.onkeydown = function(e) { keys.add(getChar(e)); }
	//Handle KeyUP
	window.onkeyup = function(e) { keys.remove(getChar(e)); }
	
	
	var drawUI = function() {
		CTX.strokeRect(play_area.x-1, play_area.y, play_area.width+2, play_area.height+1);

		CTX.fillStyle = "#FAFAFA";
		if (hold_piece == null) CTX.fillRect(0,0,play_area.x - 1, (BLOCK_SIZE*5));
		CTX.strokeRect(0, 0, (BLOCK_SIZE*5)-1, (BLOCK_SIZE*5));
	}

	

	var Run = function() {
		var c_time = new Date().getTime();
		
		//Start new game {space}
		if (!isRunning && keys.has(32)) {
			newGame();
			last_drop = c_time+300; //Make sure no harddrive happens right away
		}
		
		if (current_piece == null) return;
		
		
		// Handle Movement {A,S,D}
		if (c_time > last_movement + KEY_MOVE_LR_TIME) {
			if (keys.has(68)) {
				current_piece.moveRight();
				last_movement = c_time;
			}
			if (keys.has(65)) {
				current_piece.moveLeft();
				last_movement = c_time;
			}
			if (keys.has(83)) {
				current_piece.moveDown();
			}
		}
		
		// Handle hold {Left Shift}
		if (keys.has(16)) {
			holdPiece();
		}
		
		//Handle hard drop {Space}
		if (c_time > last_drop + KEY_DROP_TIME) {
			if (keys.has(32)) {
				current_piece.hardDrop();
				last_drop = c_time;
			}
		}
		
		//Handle rotation {W}
		if (c_time > last_rotate + KEY_ROTATE_TIME) {
			if (keys.has(87)) {
				current_piece.rotate();
				last_rotate = c_time;
			}
		}
		
		//animate
		if (c_time > last_redraw + SPEED) {
			current_piece.moveDown();		
			last_redraw = c_time;
			drawUI();
		}
	}
	
	/* Return X and Y offset to center piece inside of given rect */
	var getCenterOffset = function(piece_type, rect) {
		var centered_h = PIECE_TYPES[piece_type].start[3].x - PIECE_TYPES[piece_type].start[0].x + 1;
		centered_h = ((rect.width / 2) - (centered_h * BLOCK_SIZE) / 2) - ((piece_type == 1) ? BLOCK_SIZE : 0); //handle center h (Special for O)
		var centered_v = ((rect.height / 2) - (((piece_type == 2) ? 3 : 2) * BLOCK_SIZE) / 2); //center v (special for I)
		return {x:centered_h, y:centered_v};
	}
	
	/* Hold a piece */
	var holdPiece = function() {
		if (hold_lock) return;
		if (hold_piece == null) {
			hold_piece = new Piece(current_piece.type);
			current_piece.clear();
			newPiece();
		} else {
			var temp_type = hold_piece.type;
			hold_piece = new Piece(current_piece.type);
			current_piece.clear();
			current_piece = new Piece(temp_type);
		}
		hold_lock = true;
		
		

		var centered = getCenterOffset(hold_piece.type, {width:(BLOCK_SIZE*5), height:(BLOCK_SIZE*5)});
		CTX.fillStyle = "#FAFAFA";
		CTX.fillRect(0,0, (BLOCK_SIZE*5), (BLOCK_SIZE*5));
		hold_piece.draw({x:centered.x, y:centered.y});
	}
	
	/* Draw next 3 pieces */
	var drawFuture = function() {
	
		for(var i = 0; i < 3; i++) {
			var temp_piece = new Piece(bag[i]);
			var center = getCenterOffset(temp_piece.type, {width: (BLOCK_SIZE*5), height: (BLOCK_SIZE*5)});
			CTX.fillStyle = "#FAFAFA";
			CTX.fillRect((BLOCK_SIZE*5) + play_area.width, (BLOCK_SIZE*5)*i, (BLOCK_SIZE*5), (BLOCK_SIZE*5));
			CTX.strokeRect((BLOCK_SIZE*5) + play_area.width+1, (BLOCK_SIZE*5)*i, (BLOCK_SIZE*5)-2, (BLOCK_SIZE*5));
			temp_piece.draw({x: (BLOCK_SIZE*5) + play_area.width + center.x, y:(BLOCK_SIZE*5)*i + center.y});
		}

		
	}
	
	
	
	//Look through grid and clear finished lines
	var checkLines = function() {
		var linesCleared = 0;

		for(var y = 0; y < GRID_Y; y++) {
			var rect_pieces = new Array();
			for(var x = 0; x < GRID_X; x++) {
				/* Not above threshhold and missing space so break */
				if (game_grid[x][y] == -1 && y >= GRID_Y_HIDDEN) break;
				
				/* There is a piece above threshold so game over. */
				if (game_grid[x][y] != -1 && y < GRID_Y_HIDDEN) {
					gameOver();
					return false;
				}
				
				/* Line contains blocks in every grid space */
				if (x == GRID_X-1) linesCleared += clearLine(y);
			}
		}
		
		//If there was any lines cleared from grid array, redraw entire board 
		if (linesCleared > 0) redrawBoard();
		return true;
		
	}
	
	var clearLine = function(y) {
		for(var y2 = y; y2 >= 0; y2--) {
			for(var x=0; x < GRID_X; x++) {
				if (y2 == 0) {
					game_grid[x][y2] = -1;
				} else {
					game_grid[x][y2] = game_grid[x][y2-1];
				}
			}
		}
		
		return 1;
	}
	
	
	var redrawBoard = function() {
		CTX.clearRect(play_area.x, play_area.y, play_area.width, play_area.height);

		for(var y = GRID_Y_HIDDEN; y < GRID_Y; y++) {
			for(var x = 0; x < GRID_X; x++) {
				if (game_grid[x][y] != -1) {
					CTX.fillStyle = PIECE_COLORS[game_grid[x][y]];
					CTX.fillRect(play_area.x + x*BLOCK_SIZE, y*BLOCK_SIZE-(GRID_Y_HIDDEN*BLOCK_SIZE), BLOCK_SIZE, BLOCK_SIZE);
					CTX.strokeRect(play_area.x + x*BLOCK_SIZE+1, y*BLOCK_SIZE-(GRID_Y_HIDDEN*BLOCK_SIZE)+1, BLOCK_SIZE-2, BLOCK_SIZE-2);
				}
			}
		}
	}
	
	/* Tetris "random bag generator */
	var generateBag = function() {
		var x = new Array(0,1,2,3,4,5,6);
		x.sort(function() {return 0.5 - Math.random()});
		while(x.length > 0) {
			bag.push(x.splice(0,1)[0]);
			x.sort(function() {return 0.5 - Math.random()});
		}
	}
	
	/* Refresh random-bag if it is getting low and create a new current piece */
	var newPiece = function() {
		if (bag == null || bag.length <= 3) {
			generateBag();
		}
		
		current_piece = new Piece(bag.splice(0,1)[0], {x:3,y:0});
		
		drawFuture();
		hold_lock = false;
	}
	
	/* Game Over */
	var gameOver = function() {
		console.log("GAME OVER");
		isRunning = false;
	}
	
	var newGame = function() {
		bag = new Array();
		
		//Initialize game grid
		for(var x=0; x < GRID_X; x++) {
			game_grid[x] = new Array();
			for(var y = 0; y < GRID_Y+GRID_Y_HIDDEN; y++) {
				game_grid[x][y] = -1;
			}
		}
		
		//Redraw board (clears previous game if exists)
		redrawBoard();
		
		isRunning = true;
		newPiece();
	}
	
	/* Lock piece in */
	var mergePiece = function() {
		current_piece.mergeToBoard();
		if (!checkLines()) return;
		lock_delay_timer = null;
		newPiece();
	}
	
	var Piece = function(type, grid_offset) {
		this.type = type;
		this.p_ghost = null;
		var grid_offset = typeof grid_offset !== "undefined" ? grid_offset : {x:0,y:0}; //Set parameter default for grid_offset
		var my = this;
		my.rects = new Array();
		
		var rotation = 0;
		
		/* Push parts of the piece into rects using initial offset */
		for(var i = 0 ; i < PIECE_TYPES[type].start.length; i++) {
			my.rects.push(new Rectangle(BLOCK_SIZE * (PIECE_TYPES[type].start[i].x + grid_offset.x), BLOCK_SIZE*(PIECE_TYPES[type].start[i].y + grid_offset.y), BLOCK_SIZE, BLOCK_SIZE));
		}
		
		/* Merge this piece to the game grid (All done with it) */
		this.mergeToBoard = function() {
			for(var i=0; i < my.rects.length; i++) {
				var x = Math.floor(my.rects[i].x / BLOCK_SIZE);
				var y = Math.floor(my.rects[i].y / BLOCK_SIZE);
				game_grid[x][y] = my.type;
			}
		}
		
		/* Drop the piece into place and merge it right away */
		this.hardDrop = function() {
			this.clear();
			
			//Snap to vertical grid block
			for(var i=0; i < my.rects.length; i++) {
				my.rects[i].y = my.rects[i].y - (my.rects[i].y % BLOCK_SIZE);
			}
			
			//Move down until can't anymore
			while(canMove("down", my.rects)) {
				for(var i=0; i < my.rects.length; i++) {
					my.rects[i].y += BLOCK_SIZE;
				}
			}
			
			//Draw and merget to board
			my.draw();
			mergePiece();
		}
		
		
		/* Draw the piece (To board or offset) */
		this.draw = function(offset) {
			CTX.fillStyle = PIECE_COLORS[my.type];
			
			//If no offset is given draw to board
			if (typeof(offset) == "undefined" || offset == null) {
				drawGhost();			
				//Draw Piece
				for(var i=0; i < my.rects.length; i++) {
					CTX.fillRect(play_area.x + my.rects[i].x, my.rects[i].y-(GRID_Y_HIDDEN*BLOCK_SIZE), my.rects[i].width, my.rects[i].height);
					CTX.strokeRect(play_area.x + my.rects[i].x+1, my.rects[i].y-(GRID_Y_HIDDEN*BLOCK_SIZE)+1, my.rects[i].width-2, my.rects[i].height-2);	
				}
			} else {
				for(var i=0; i < my.rects.length; i++) {
					CTX.fillRect(my.rects[i].x+offset.x, my.rects[i].y+offset.y, my.rects[i].width, my.rects[i].height);
					CTX.strokeRect(my.rects[i].x+offset.x+1, my.rects[i].y+offset.y+1, my.rects[i].width-2, my.rects[i].height-2);	
				}
			}
		}
		
		/* Draw the ghost piece */
		var drawGhost = function() {
			var ghost = new Array();

			//Copy rect into ghost
			for(var i=0; i < my.rects.length; i++) {
				ghost.push(new Rectangle(my.rects[i].x, my.rects[i].y, my.rects[i].width, my.rects[i].height));
			}
		
			//Set ghost to snap to grid
			for(var i = 0; i < ghost.length; i++) {			
				ghost[i].y = ghost[i].y - (ghost[i].y % BLOCK_SIZE);
			}
			
			//Drop ghost to bottom
			while(canMove("down", ghost)) {
				for(var i=0; i < ghost.length; i++) {
					ghost[i].y += BLOCK_SIZE;
				}
			}
			
			//Draw ghost
			for(var i=0; i < ghost.length; i++) {
				CTX.strokeRect(play_area.x + ghost[i].x+1, ghost[i].y-(GRID_Y_HIDDEN*BLOCK_SIZE)+1, ghost[i].width-2, ghost[i].height-2);
			}
			my.p_ghost = ghost;
		}
		
		
		//Move piece right if possible
		this.moveRight = function() {
			if (!canMove("right", my.rects)) return;
			this.clear();
			for(var i=0; i < my.rects.length; i++) {
				my.rects[i].x += BLOCK_SIZE;
			}
			my.draw();
		}
		
		//Move piece left if possible
		this.moveLeft = function() {
			if (!canMove("left", my.rects)) return;
			this.clear();
			for(var i=0; i < my.rects.length; i++) {
				my.rects[i].x -= BLOCK_SIZE;
			}
			my.draw();
		}
		
		//Move piece down if possible. If not create a new piece!
		this.moveDown = function() {
			if (!canMove("down", my.rects)) {
				if (lock_delay_timer == null) {
					lock_delay_timer = setTimeout(mergePiece, LOCK_DELAY);
				}
				return;
			}
			this.clear();
			for(var i=0; i < my.rects.length; i++) {
				my.rects[i].y += MOVE_DOWN_PIXELS;
			}
			
			if (lock_delay_timer != null) {
				clearTimeout(lock_delay_timer);
				lock_delay_timer = null;
			}
			my.draw();
		}
		
		this.clear = function() {
			//Clear block
			for(var i=0; i < my.rects.length; i++) {
				CTX.clearRect(play_area.x + my.rects[i].x, my.rects[i].y-(GRID_Y_HIDDEN*BLOCK_SIZE), my.rects[i].width, my.rects[i].height);
			}
			
			//Clear previous ghost if exists
			if (my.p_ghost != null) {
				for(var i=0; i < my.p_ghost.length; i++) {
					CTX.clearRect(play_area.x + my.p_ghost[i].x, my.p_ghost[i].y-(GRID_Y_HIDDEN*BLOCK_SIZE), my.p_ghost[i].width, my.p_ghost[i].height);
				}
			}
		}
	
		//Rotate piece if possible
		this.rotate = function() {
			if (!canRotate(my.rects)) return;
			this.clear();
						
			for(var i=0; i < my.rects.length; i++) {
				my.rects[i].x += PIECE_TYPES[my.type].rotations[rotation][i].x*BLOCK_SIZE;
				my.rects[i].y += PIECE_TYPES[my.type].rotations[rotation][i].y*BLOCK_SIZE;
			}
			
			my.draw();
			rotation = (rotation < PIECE_TYPES[my.type].rotations.length - 1) ? rotation + 1 : 0;
		}
		
		
		/* Check if it is possible to rotate */
		var canRotate = function(rect) {
			for(var r = 0; r < rect.length; r++) {
				var temp_rect = new Rectangle(rect[r].x + PIECE_TYPES[my.type].rotations[rotation][r].x*BLOCK_SIZE,
											  rect[r].y + PIECE_TYPES[my.type].rotations[rotation][r].y*BLOCK_SIZE, 
											  BLOCK_SIZE, 
											  BLOCK_SIZE);				
				if (!validMovement(temp_rect)) return false;	
			
			}
			
			return true;
		}
				
		/* Check if it is possible to move */
		var canMove = function(dir, rect) {
			var addLR = 0;
			var addUD = 0;
			switch(dir) {
				case "left": addLR = -BLOCK_SIZE; break;
				case "right": addLR = BLOCK_SIZE; break;
				case "down": addUD = MOVE_DOWN_PIXELS; break;
			}
			for(var r = 0; r < rect.length; r++) {
				var temp_rect = new Rectangle(rect[r].x+addLR, rect[r].y+addUD, rect[r].width, rect[r].height);
				if (!validMovement(temp_rect)) return false;		
			}
			return true;
		}
		
		/* Check if position of rect is valid/allowed */
		var validMovement = function(rect) {
			//Check if movement goes off play area
			if (rect.x + rect.width > play_area.width || rect.x < 0 ||
				rect.y+rect.height > play_area.height + (GRID_Y_HIDDEN * BLOCK_SIZE)) {
					return false;
			}	

			//Check if movement collides with another piece
			var x = Math.floor(rect.x / BLOCK_SIZE);
			var x2 = Math.ceil(rect.x / BLOCK_SIZE);
			var y = Math.floor(rect.y / BLOCK_SIZE);
			var y2 = Math.ceil(rect.y / BLOCK_SIZE);
			
			if (game_grid[x][y] != -1 || game_grid[x2][y2] != -1) {
				if (y < GRID_Y_HIDDEN-1 || y2 < GRID_Y_HIDDEN-1) return true; //Let stuff rotate/move off the top
				return false;
			}

			return true;
		}
		
	}
	

	CTX.fillText("<SPACE> to begin.",150,200);
	drawUI();
	game_loop = setInterval(Run, "4");	
}


//Rectangle intersection function
function Intersects(r1, r2) {
  return !(r2.x > r1.x + r1.width-1 || 
           r2.x + r2.width-1 < r1.x || 
           r2.y > r1.y + r1.height-1 ||
           r2.y + r2.height-1 < r1.y);
}

function Rectangle(x,y,w,h) {
	this.x = x;
	this.y = y;
	this.width = w;
	this.height = h;
}

//Container to hold keys. For improved key handling.
function KeyContainer() {
	var keys = new Array();
	this.length = 0;
	
	//Add key to container if isn't already in it.
	this.add = function(key) {
		if (!this.has(key)) {
			this.length++;
			keys.push(new Key(key));
		}
	}

	//Get a key (index)
	this.get = function(index) {
		return keys[index];
	}
	
	//Check if has key
	this.has = function(key) {
		for(var i=0; i < keys.length; i++) {
			if (keys[i].code == key) return true;
		}
		return false;
	}
	
	//Remove key
	this.remove = function(key) {
		for(var i=0; i < keys.length; i++) {
			if (keys[i].code == key) {
				keys.splice(i,1);
				this.length--;
				return;
			}
		}
	}
}
function Key(code) {
	this.code = code;
}
function getChar(event) {
	if (event.which == null) {
		return event.keyCode // IE
	} else if (event.which!=0 && event.charCode!=0) {
		return event.which   // the rest
	} else {
		return event.which
	}
}

function rand(min, max) {
	return Math.floor(Math.random() * (max - min + 1)) + min;
}
function clone(obj) {
    if (null == obj || "object" != typeof obj) return obj;
    var copy = obj.constructor();
    for (var attr in obj) {
        if (obj.hasOwnProperty(attr)) copy[attr] = obj[attr];
    }
    return copy;
}